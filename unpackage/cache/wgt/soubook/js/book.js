function book(num) {
	mui.openWindow({
		url: "book.html",
		id: "book.html",
		extras: { //自定义扩展参数，可以用来处理页面间传值
			num: num
		},
		createNew: false, //是否重复创建同样id的webview，默认为false:不重复创建，直接显示
		show: {
			autoShow: true //页面loaded事件发生后自动显示，默认为true/页面动画持续时间，Android平台默认100毫秒，iOS平台默认200毫秒；
		},
		waiting: {
			autoShow: false, //自动显示等待框，默认为true
			title: '正在加载...' //等待对话框上显示的提示内容
		}
	});
}

function chapter(key,bookName,bookId,bookNum,chapterNum,chapterName) {
	if (key == "") return;
	mui.openWindow({
		url: "chapter.html",
		id: "chapter.html",
		extras: { //自定义扩展参数，可以用来处理页面间传值
			key: key,
			bookName: bookName,
			bookId: bookId,
			bookNum: bookNum,
			chapterNum: chapterNum,
			chapterName: chapterName
		},
		createNew: false, //是否重复创建同样id的webview，默认为false:不重复创建，直接显示
		show: {
			autoShow: true //页面loaded事件发生后自动显示，默认为true/页面动画持续时间，Android平台默认100毫秒，iOS平台默认200毫秒；
		},
		waiting: {
			autoShow: false, //自动显示等待框，默认为true
			title: '正在加载...' //等待对话框上显示的提示内容
		}
	});
}
