# 搜书

#### 介绍
搜书是一款纯净的小说阅读App，拥有书籍搜索，书籍阅读，书架收藏等基本功能。

后端程序项目 

[eara/book-api](https://gitee.com/eara/book-api)

演示App下载

![搜书下载](download.png)

#### 软件架构
使用MUI开发


#### 安装教程

1.  [MUI学习网站](https://dev.dcloud.net.cn/mui/)
2.  使用HbuildX导入代码 [HbuildX使用说明](https://hx.dcloud.net.cn/README)
3.  修改js/config.js中的后台地址
4.  使用HbuildX云打包功能或自己离线打包

#### 使用说明

1.  首页
    ![首页](https://images.gitee.com/uploads/images/2021/1216/103353_77a4c084_2147852.jpeg "Screenshot_2021-12-16-10-29-03-781_plus.soubook.jpg")
2.  书籍详情
    ![书籍详情](https://images.gitee.com/uploads/images/2021/1216/103433_9f0bdab8_2147852.jpeg "Screenshot_2021-12-16-10-29-48-539_plus.soubook.jpg")
3.  章节阅读
    ![章节阅读](https://images.gitee.com/uploads/images/2021/1216/103502_a62a7623_2147852.jpeg "Screenshot_2021-12-16-10-30-01-881_plus.soubook.jpg")
4.  阅读设置
    ![阅读设置](https://images.gitee.com/uploads/images/2021/1216/103521_d0e5ed89_2147852.jpeg "Screenshot_2021-12-16-10-30-13-240_plus.soubook.jpg")

#### 说明
本项目仅为学习MUI技术开发，请勿用作商业用途