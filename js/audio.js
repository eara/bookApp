var tokenUrl = "https://openapi.baidu.com/oauth/2.0/token";
var client_id = "eK4G3sSYCQgqypupZfTYcWHX"; //此处为申请的client_id;
var client_secret = "0Z1HxYw9j7BiRuTMVUFLgHmLfCHKMu6Y"; //此处为申请的client_secret ;
var access_token;
var data = "grant_type=client_credentials&client_id=" + client_id + "&client_secret=" + client_secret;
var p = document.createElement("audio"); //创建一个潜在的audio播放器
var p2 = document.createElement("audio"); //创建一个潜在的audio播放器
var contentArray = new Array();
var chapterId;
var speed = 6;
var playbackRate = 1;
var a = 0,
	b = 0,
	c = 0;
var tex;
var cp;

function addText(text) {
	tex = text.replace(/[\r\n]/g, "").replace(/\ +/g, "").replace(/-/g, '').trim(); //对文本进行去空格和换行；
	if (tex.length / 500 >= 0) { //接口上传限制字数
		for (var i = 0; i < tex.length / 500; i++) {
			a = a + 500;
			splitTex = tex.slice(b, a);
			b = a;
			contentArray.push(splitTex);
		}
	}
}

function clear() {
	a = 0;
	b = 0;
	c = 0;
	contentArray = [];
}

function play(id) {
	plus.nativeUI.showWaiting("加载中...");
	chapterId = id;
	mui.ajax({
		type: "get",
		url: tokenUrl,
		data: data,
		async: true,
		success: function(resp) {
			if (resp.access_token) {
				access_token = resp.access_token;
				var shibieUrl = "http://tsn.baidu.com/text2audio";
				tex = encodeURI(contentArray[0]);
				var data = "tex=" + tex + "&tok=" + access_token +
					"&cuid=00:00:00:00:00:00&ctp=1&lan=zh&spd=" + speed + "&pit=5&vol=5&per=0&aue=3";
				p.src = shibieUrl + "?" + data;
				p.play();
				cp = p;
				tex = encodeURI(contentArray[1]);
				var data2 = "tex=" + tex + "&tok=" + access_token +
					"&cuid=00:00:00:00:00:00&ctp=1&lan=zh&spd=" + speed + "&pit=5&vol=5&per=0&aue=3";
				p2.src = shibieUrl + "?" + data2;
				plus.nativeUI.closeWaiting();
				c = c + 2;
			} else {}
		},
		error: function(error) {}
	});
	p.addEventListener("ended", function() {
		getNext();
		if (contentArray.length > 0 && c < contentArray.length) {
			p2.play();
			p2.playbackRate = playbackRate;
			cp = p2;
			var shibieUrl = "http://tsn.baidu.com/text2audio";
			tex = encodeURI(contentArray[c]);
			var data = "tex=" + tex + "&tok=" + access_token +
				"&cuid=00:00:00:00:00:00&ctp=1&lan=zh&spd=" + speed + "&pit=5&vol=5&per=0&aue=3";
			p.src = shibieUrl + "?" + data;
			c++;
		}
	});
	p2.addEventListener("ended", function() {
		getNext();
		if (contentArray.length > 0 && c < contentArray.length) {
			p.play();
			p.playbackRate = playbackRate;
			cp = p;
			var shibieUrl = "http://tsn.baidu.com/text2audio";
			tex = encodeURI(contentArray[c]);
			var data = "tex=" + tex + "&tok=" + access_token +
				"&cuid=00:00:00:00:00:00&ctp=1&lan=zh&spd=" + speed + "&pit=5&vol=5&per=0&aue=3";
			p2.src = shibieUrl + "?" + data;
			c++;
		}
	});
	
}

function setSpeed(pb){
	playbackRate = pb;
}

function getNext() {
	if (c >= (contentArray.length - 1)) {
		clear();
		if (chapterId == "") {
			return;
		}
		mui.ajax(config.config.path + "/chapter/" + chapterId, {
			type: "GET",
			dataType: "json",
			async:false,
			success: function(result) {
				var code = result.code;
				if (code == 200) {
					var Days = 30;
					var exp = new Date();
					exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
					var temp = bookName + ':' + result.data.title + ':' + chapterId + ';expires=' + exp
						.toGMTString();
					plus.navigator.setCookie('agoInfo', temp);

					mui.ajax(config.config.path + "/book-tag", {
						data: {
							"bookId": 0,
							"bookName": bookName,
							"bookNumber": result.data.bookNumber,
							"chapterId": next,
							"chapterTitle": result.data.title,
							"deviceNum": deviceNum
						},
						dataType: 'json', //服务器返回json格式数据
						type: 'POST', //HTTP请求类型
						contentType: 'application/json;charset=utf-8',
						success: function(data) {}
					});
					var html = "<p><br><strong>" + result.data.title + "</strong></p>" + result.data.content;
					var div = document.createElement('div');
					var table = document.body.querySelector('#current');
					table.id = "";
					div.id = "current";
					div.className = table.className;
					div.innerHTML = html;
					table.append(div);
					var y = document.getElementById("current").offsetTop;
					mui("#body2").scroll().scrollTo(0, -y, 100);
					
					addText(div.textContent);
					chapterId = result.data.next;
				} else {
					chapterId = "";
					addText("已到最后一章");
					var table = document.body.querySelector('#load');
					table.innerHTML = "已到最后一章";
				}
			}
		});
	}
}

function pause() {
	if (cp.paused) {
		cp.play();
	} else {
		cp.pause();
	}
}
