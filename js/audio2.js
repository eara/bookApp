var tokenUrl = "https://openapi.baidu.com/oauth/2.0/token";
var client_id = "eK4G3sSYCQgqypupZfTYcWHX"; //此处为申请的client_id;
var client_secret = "0Z1HxYw9j7BiRuTMVUFLgHmLfCHKMu6Y"; //此处为申请的client_secret ;
var access_token;
var data = "grant_type=client_credentials&client_id=" + client_id + "&client_secret=" + client_secret;
var p = document.createElement("audio"); //创建一个潜在的audio播放器
var contentArray = new Array();
var chapterId;
var speed = 6;
var a = 0,
	b = 0,
	c = 0;
var tex;
function addText(text){
	tex = text.replace(/[\r\n]/g, "").replace(/\ +/g, "").replace(/-/g, '').trim(); //对文本进行去空格和换行；
	if (tex.length / 100 >= 0) { //接口上传限制字数，避免出现接口腻出，限制上传字数
		for (var i = 0; i < tex.length / 100; i++) {
			a = a + 100;
			splitTex = tex.slice(b, a);
			b = a;
			contentArray.push(splitTex);
		}
	}
}
function clear(){
	a = 0;
	b = 0;
	c = 0;
	contentArray = new Array();
}
function play(id) {
	plus.nativeUI.showWaiting("加载中..."); 
	chapterId = id;
	mui.ajax({
		type: "get",
		url: tokenUrl,
		data: data,
		async: true,
		success: function(resp) {
			if (resp.access_token) {
				access_token = resp.access_token;
				var shibieUrl = "http://tsn.baidu.com/text2audio";
				tex = encodeURI(contentArray[0]);
				var data = "tex=" + tex + "&tok=" + access_token +
					"&cuid=00:00:00:00:00:00&ctp=1&lan=zh&spd="+speed+"&pit=5&vol=5&per=0&aue=3";
				p.src = shibieUrl + "?" + data;
				p.autoplay = "autoplay";
				// p.play();
				plus.nativeUI.closeWaiting();
				c++;
			} else {}
		},
		error: function(error) {}
	});
		p.addEventListener("ended",function() {
			plus.nativeUI.showWaiting("加载中...");
			if (contentArray.length > 0 && c < contentArray.length) {
				var shibieUrl = "http://tsn.baidu.com/text2audio";
				tex = encodeURI(contentArray[c]);
				var data = "tex=" + tex + "&tok=" + access_token +
					"&cuid=00:00:00:00:00:00&ctp=1&lan=zh&spd="+speed+"&pit=5&vol=5&per=0&aue=3";
				p.src = shibieUrl + "?" + data;
				p.play();
				plus.nativeUI.closeWaiting();
				c++;
				if(c == contentArray.length){
					clear();
					if (chapterId == "") {
						return;
					}
					mui.ajax(config.config.path + "/chapter/" + chapterId, {
						type: "GET",
						dataType: "json",
						success: function(result) {
							var code = result.code;
							if (code == 200) {
								var Days = 30;
								var exp = new Date();
								exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
								var temp = bookName + ':' + result.data.title + ':' + chapterId + ';expires=' + exp.toGMTString();
								plus.navigator.setCookie('agoInfo', temp);
								
								var html = "<br>" + result.data.title + "<hr>" + result.data.content;
								var div = document.createElement('div');
								div.className = "font";
								div.innerHTML = html;
								addText(div.textContent);
								chapterId = result.data.next;
							} else {
								chapterId = "";
								addText("已到最后一章");
							}
						}
					});
				}
			} 
		});
}
function pause() {
	if (p.paused) {
		p.play();
	} else {
		p.pause();
	}

}
