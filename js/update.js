(function(w) {
	var wgtVer = null;
	var version = null;

	function plusReady() {
		// ......  
		// 获取本地应用资源版本号  
		plus.runtime.getProperty(plus.runtime.appid, function(inf) {
			wgtVer = inf.version;
			console.log("当前应用版本：" + wgtVer);
			plus.storage.setItem("version", wgtVer);
			checkUpdate();
		});
	}
	if (window.plus) {
		plusReady();
	} else {
		document.addEventListener('plusready', plusReady, false);
	}

	// 检测更新  
	var checkUrl = config.config.path + "/app/checkUpdate";

	function checkUpdate() {
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function() {
			switch (xhr.readyState) {
				case 4:
					plus.nativeUI.closeWaiting();
					if (xhr.status == 200) {
						var resp = JSON.parse(xhr.responseText);
						if (resp.code != '200') return;
						var appUpdate = resp.data;
						version = appUpdate.version;
						console.log("检测更新成功：" + version);
						var updateNotice = appUpdate.notice;
						if(updateNotice){
							updateNotice = "更新内容：\n" + updateNotice;
						}else{
							updateNotice = " "; 
						}
						var length = appUpdate.fileSize;
						var unit = "B";
						if(length > 1000 && length < 1000000){
							unit = "KB";
							length = length / 1000;
						}else if(length >= 1000000){
							unit = "MB";
							length = length / 1000 / 1000;
						}
						updateNotice = updateNotice + "\n更新大小：" + length.toFixed(2) + unit;
						if (wgtVer && version && (wgtVer != version)) {
							if(appUpdate.require == 0){
								//可选更新
								plus.nativeUI.confirm(updateNotice, function(i) {
									if (0 == i.index) {
										downWgt(appUpdate.url,appUpdate.type); // 下载升级包
									} else {
										return;
									}
								}, "新版本 v" + version, ["立即更新", "取　　消"]);
							}else{
								//强制更新
								plus.nativeUI.confirm(updateNotice, function(i) {
									if (0 == i.index) {
										downWgt(appUpdate.url,appUpdate.type); // 下载升级包
									} else {
										plus.runtime.quit();
									}
								}, "新版本 v" + version, ["立即更新", "退　　出"]);
							}
						} else {}
					} else {
						console.log("检测更新失败！");
					}
					break;
				default:
					break;
			}
		}
		xhr.open('GET', checkUrl);
		xhr.send();
	}

	function checkUpdate2() {
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function() {
			switch (xhr.readyState) {
				case 4:
					plus.nativeUI.closeWaiting();
					if (xhr.status == 200) {
						console.log("检测更新成功：" + xhr.responseText);
						var newVer = xhr.responseText;
						if (newVer == '暂无app版本信息') return;
						version = newVer.split("@")[0];
						var updateNotice = newVer.split("@")[1];
						var length = newVer.split("@")[2];
						if(updateNotice){
							updateNotice = "更新内容：\n" + updateNotice;
						}else{
							updateNotice = " "; 
						}
						updateNotice = updateNotice + "\n更新大小：" + length + "KB";
						if (wgtVer && version && (wgtVer != version)) {
							plus.nativeUI.confirm(updateNotice, function(i) {
								if (0 == i.index) {
									downWgt(); // 下载升级包
								} else {
									return;
								}
							}, "新版本 v" + version, ["立即更新", "取　　消"]);
						} else {}
					} else {
						console.log("检测更新失败！");
					}
					break;
				default:
					break;
			}
		}
		xhr.open('GET', checkUrl);
		xhr.send();
	}
	
	// 下载wgt文件  
	var wgtUrl = config.config.path + "/app/update";

	function downWgt(file,type) {
		plus.nativeUI.showWaiting("下载资源文件...");
		plus.downloader.createDownload(wgtUrl+"?file="+file, {
			filename: "_doc/update/"+file
		}, function(d, status) {
			if (status == 200) {
				console.log("下载资源成功：" + d.filename);
				if(type == 0){
					//apk
					plus.runtime.install(d.filename);  // 安装下载的apk文件
					plus.runtime.quit();
				}else{
					installWgt(d.filename); // 安装wgt包
				}
			} else {
				console.log("下载资源失败！");
				plus.nativeUI.alert("下载资源失败！");
			}
			plus.nativeUI.closeWaiting();
		}).start();
	}

	// 更新应用资源  
	function installWgt(path) {
		plus.nativeUI.showWaiting("安装资源文件...");
		plus.runtime.install(path, {}, function() {
			plus.nativeUI.closeWaiting();
			console.log("安装资源文件成功！");
			plus.nativeUI.alert("更新完成！请重新启动应用", function() {
				plus.storage.setItem("version", version);
				plus.runtime.quit();
			});
		}, function(e) {
			plus.nativeUI.closeWaiting();
			console.log("安装资源文件失败[" + e.code + "]：" + e.message);
			plus.nativeUI.alert("安装资源文件失败[" + e.code + "]：" + e.message);
		});
	}


})(window);
